#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main()
{
    clock_t start, end;
    static int a[1024][1024], b[1024][1024], product[1024][1024];
    int prime_count;
    int arow, acol, brow, bcol, i, j, k;
    int sum = 0;
    // matrix A
    printf("Enter the row and columns of matrix a : ");
    scanf("%d %d", &arow, &acol);
    printf("Random number of matrix a \n\n");
    printf("<-------------Loadind-------------> \n\n");
    for (i = 0; i < arow; i++)
    {
        for (j = 0; j < acol; j++)
        {
            a[i][j] = rand() % 10;
        }
    }
    // matrix B
    printf("Enter the row and columns of matrix b : ");
    scanf("%d %d", &brow, &bcol);
    if (brow != acol)
    {
        printf("===============================================");
        printf("Error : Can't mutiply the matrix a and b !!!!!!");
        printf("===============================================");
    }
    else
    {
        printf("Random number of matrix b : \n\n");
        printf("<-------------Loadind-------------> \n\n");
        for (i = 0; i < brow; i++)
        {
            for (j = 0; j < bcol; j++)
            {
                b[i][j] = rand() % 10;
            }
        }
    }
    printf("\n");
    // mutiply the matrix a and b
    for (i = 0; i < arow; i++)
    {
        for (j = 0; j < bcol; j++)
        {
            for (k = 0; k < bcol; k++)
            {
                sum += a[i][k] * b[k][j];
            }
            product[i][j] = sum;
            sum = 0;
        }
    }
    // Start timer
    start = clock();
    printf("Result matrix \n");
    for (i = 0; i < arow; i++)
    {
        for (j = 0; j < bcol; j++)
        {
            printf("%d ", product[i][j]);
        }
        printf("\n");
    }
    end = clock();
    float sec = (float)(end - start) / CLOCKS_PER_SEC;
    printf("There are %d prime numbers between 1 - 100000\n", prime_count);
    printf("%d clocks ticked\n", end - start);
    printf("Calculation finished in %f seconds\n", sec);
    return 0;
}